﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] private float width;
    [SerializeField] private float minWidth;
    [SerializeField] private float maxWidth;
    [SerializeField] private float spacing;
    [SerializeField] private GameObject enemy;
    [SerializeField] private GameObject enemy2;
    private bool changer = true;


    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("SpawnerFunction", 0.5f, 1);
    }

    void SpawnerFunction()
    {
        width = Random.Range(minWidth, maxWidth);

        if (changer)
        {
            Instantiate(enemy, new Vector3(width, transform.position.y * spacing, 0), Quaternion.identity);
            changer = false;
        }
        else
        {
            Instantiate(enemy2, new Vector3(width, transform.position.y * spacing, 0), Quaternion.identity);
            changer = true;
        }

        


    }


}
