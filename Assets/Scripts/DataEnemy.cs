﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataEnemy : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] public int health = 1;
    [SerializeField] private int scoreDeath;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (health <= 0)
        {
            Destroy(gameObject);
            GameManager.Instance.score += scoreDeath;
        }
    }

    private void FixedUpdate()
    {
        transform.Translate(Vector3.down * speed * Time.deltaTime);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == ("Death") )
        {
            Debug.Log("dintre");
            Destroy(gameObject);

            //SceneManager.LoadScene("M17UF1-Prova");


        }
    }
}
