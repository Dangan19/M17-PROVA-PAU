﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fragate : MonoBehaviour
{
    [SerializeField] private Transform firePoint1;
    [SerializeField] private Transform firePoint2;
    [SerializeField] private GameObject bullet;

    private bool right = false;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("Shoot", 2.0f, 1f);
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void FixedUpdate()
    {

    }

    void Shoot()
    {
        if (right)
        {
            Instantiate(bullet, firePoint1.position, firePoint1.rotation);
            right = false;
        }
        else
        {
            Instantiate(bullet, firePoint2.position, firePoint2.rotation);
            right = true;
        }
      

    }
}
